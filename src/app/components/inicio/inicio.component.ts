import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AngularFireDatabase } from '@angular/fire/database';
import { DeseosService } from 'src/app/services/deseos.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Deseos } from '../../interfaces/deseos';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.sass'],
})
export class InicioComponent implements OnInit {
  cerra = false;
  formDeseo: FormGroup;
  submitted = false;
  mensaje: string = null;
  activo: string = null;
  canmensaje: number;
  ColumnsSlide: number;
  mobWidth: any;
  slideConfig: any;
  datenow: Date = new Date();
  daysNov: number = 30;
  daysDec: number = 24;
  daterest: number = 0;
  anuncio: string;
  cantCaracter: number = 150;
  lista = [];
  urlNovena: String = "/compartir-novena";
  slides = [];
  deseos: Deseos[];
  tokenUser: string;
  dataUser;

  constructor(private formBuilder: FormBuilder, 
    private angularFireDatabase: AngularFireDatabase, 
    private deseosService: DeseosService, 
    private router:Router, private rutasActivas: ActivatedRoute) {
      this.tokenUser = this.rutasActivas.snapshot.params['token'];
      if(this.tokenUser != null && this.tokenUser != undefined){
        this.tokenUser = this.tokenUser.replace(/\*SLASH\*/g,"/");
        this.tokenUser = this.tokenUser.replace(/\*MAS\*/g,"+");
        this.tokenUser = this.tokenUser.replace(/\*IGUAL\*/g,"=");
        console.log(this.tokenUser);
        this.getdataUser(this.tokenUser)
      }
    this.mobWidth = (window.screen.width);
    this.ColumnsSlide = 0;
    if (this.mobWidth < 992) {
      this.slideConfig = {"slidesToShow": 3 , "slidesToScroll": 1};
      this.slides= [{img: "assets/img/img-novena-min.png", routerlink: this.urlNovena, target:""},
                    {img: "assets/img/img-aguinaldo-min.png", routerlink: "/organiza-novena", target:""},
                    {img: "assets/img/img-eventos-min.png", routerlink: "/eventos", target:""},
                    {img: "assets/img/img-villancicos-min.png", routerlink: "https://miclaroapp.com.co/novena?url_externa=https://f63y8.app.goo.gl/claromusica", target:"_blanck"}];

      if( this.mobWidth < 576){
        this.slideConfig = {"slidesToShow": 2 , "slidesToScroll": 2};
        this.slides= [{img: "assets/img/img-novena-min.png", routerlink: this.urlNovena, target:""},
                    {img: "assets/img/img-aguinaldo-min.png", routerlink: "/organiza-novena", target:""},
                    {img: "assets/img/img-eventos-min.png", routerlink: "/eventos", target:""},
                    {img: "assets/img/img-villancicos-min.png", routerlink: "https://miclaroapp.com.co/novena?url_externa=https://f63y8.app.goo.gl/claromusica", target:"_blanck"}];

      }
    } else {
      this.slideConfig = {"slidesToShow": 4 , "slidesToScroll": 1};
      this.slides= [{img: "assets/img/img-novena-min.png", routerlink: this.urlNovena, target:""},
                    {img: "assets/img/img-aguinaldo-min.png", routerlink: "/organiza-novena", target:""},
                    {img: "assets/img/img-eventos-min.png", routerlink: "/eventos", target:""},
                    {img: "assets/img/img-villancicos-min.png", routerlink: "https://f63y8.app.goo.gl/claromusica", target:"_blanck"}];

    } 

    if (this.datenow.getMonth() == 10) {
      this.daterest = this.daysNov - this.datenow.getDate() + 24
    } if (this.datenow.getMonth() == 11) {
      this.daterest = 24 - this.datenow.getDate()
      if (this.datenow.getDate() == 25) {
        
        this.anuncio= "Feliz navidad";
      }
    }
   }
  ngOnInit() {
    this.formDeseo = this.formBuilder.group({
      sms: ['', Validators.required ],
    });
    
  }
  abrirpopod(){
    this.cerra = true;
  }
  cerrarpopod(){
    this.cerra = false;
  }
  get f(){return this.formDeseo.controls}

  enviarDeseo(){
    this.submitted  = true;
    if(this.formDeseo.invalid){
      return
    } else{
      this.dataUser = (this.dataUser)?this.dataUser:{"nombre":"Usuario Claro","apellido":"N/A","correo":"N/A","documento":"N/A","linea":"N/A"};
      const deseos = {
        mensaje: this.mensaje,
        activo: this.activo = "0",
        nombre: this.dataUser.nombre,
        apellido: this.dataUser.apellido,
        correo: this.dataUser.correo,
        documento: this.dataUser.documento,
        linea: this.dataUser.linea 
      }
      this.deseosService.addDeseo(deseos).then((data)=>{
      this.router.navigate(['deseos']);
      }).catch((error)=>{
        console.log(error);
      });
      
    }
    
  }
  
  getdataUser(token){
    let url = this.deseosService.post(token).subscribe((data) => {
      if(!data["error"] && data && data["response"] && data["response"]["usuario"] && data["response"]["cuenta"]){
        var user = {};
        user["nombre"] = data["response"]["usuario"]["nombre"];
        user["apellido"] = data["response"]["usuario"]["apellido"];
        user["documento"] = data["response"]["usuario"]["DocumentNumber"];
        user["correo"] = data["response"]["usuario"]["UserProfileID"];
        user["linea"] = data["response"]["cuenta"]["AccountId"];
        console.log(user);
        this.dataUser = user;
      }else{
        console.log("Error en el servicio")
      }
    }, (error) =>{
      console.log(error);
      alert('ocurrio un error al tratar de otener usuario');
    });
    console.log(url);
  }
  
  afterChange(e) {
    console.log('afterChange');
  }
  
  beforeChange(e) {
    console.log('beforeChange');
  }
  
  
}
