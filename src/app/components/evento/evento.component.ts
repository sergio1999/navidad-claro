import { Component, OnInit } from '@angular/core';
import { Eventos } from 'src/app/interfaces/eventos';
import { EventosService } from 'src/app/services/eventos.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-evento',
  templateUrl: './evento.component.html',
  styleUrls: ['./evento.component.sass']
})
export class EventoComponent implements OnInit {
evento: Eventos;
Idevento: any;
eventos: Eventos[];
lat: any;
lng: any;
  constructor(private eventoServices: EventosService, private activiteRoute: ActivatedRoute) {
    this.Idevento = this.activiteRoute.snapshot.params['idevento'];
    this.eventos = this.eventoServices.getEventos();
    this.evento = this.eventos.find((record) => {
      return record.idevento == this.Idevento;
    });
   }

  ngOnInit() {
    this.lat = this.evento.datalat;
    this.lng = this.evento.datalng;
  }

}
