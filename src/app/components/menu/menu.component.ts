import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { EventosService } from 'src/app/services/eventos.service';
import { Eventos } from 'src/app/interfaces/eventos';
import { Novenas } from 'src/app/interfaces/novenas';
import { NovenasService } from 'src/app/services/novenas.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.sass']
})
export class MenuComponent implements OnInit {
  subscription: Subscription;
  geturl: any;
  menuishome: boolean = true;
  buscar: string = '';
  rtaBuscar: boolean = false;
  eventos: Eventos[];
  novenas: Novenas[];
  constructor( private router: Router,
    private location: Location,
    private eventosService: EventosService,
    private novenasService: NovenasService) { 
    this.subscription = router.events.subscribe((event) => {
      this.geturl = event;
        if(this.geturl.url != undefined){
          
          if(this.geturl.url == '/' || this.geturl.url.indexOf("inicio") > -1){
            this.menuishome = true;
          }else{
            this.menuishome = false;
          }

          
          console.log('----------------urlcapture-------------------');
          console.log(this.geturl.url.indexOf("inicio"));
          console.log('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX');
        }
    });
    this.eventos = eventosService.getEventos();
    this.novenas = novenasService.getNovenas();

  }
  ngOnInit() { }

  backClicked() {
    this.location.back();
  }
 
}
