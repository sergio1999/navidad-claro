import { Component, OnInit } from '@angular/core';
import { Eventos } from 'src/app/interfaces/eventos';
import { EventosService } from 'src/app/services/eventos.service';

@Component({
  selector: 'app-eventos',
  templateUrl: './eventos.component.html',
  styleUrls: ['./eventos.component.sass']
})
export class EventosComponent implements OnInit {
  eventos: Eventos[];
  constructor(private eventosServices: EventosService) { 
    this.eventos = eventosServices.getEventos();
  }

  ngOnInit() {
  }

}
