import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumenNovenaComponent } from './resumen-novena.component';

describe('ResumenNovenaComponent', () => {
  let component: ResumenNovenaComponent;
  let fixture: ComponentFixture<ResumenNovenaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResumenNovenaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResumenNovenaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
