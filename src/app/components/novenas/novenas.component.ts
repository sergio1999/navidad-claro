import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Novenas } from 'src/app/interfaces/novenas';
import { NovenasService } from 'src/app/services/novenas.service';

@Component({
  selector: 'app-novenas',
  templateUrl: './novenas.component.html',
  styleUrls: ['./novenas.component.sass']
})
export class NovenasComponent implements OnInit {
  novenaId: any;
  infoNovena: Novenas;
  allnovena: Novenas[]
/*usamos el activadRouter para referenciar el id de la url*/
  constructor(private activedRouter: ActivatedRoute, private novenaService: NovenasService) {
    this.novenaId = this.activedRouter.snapshot.params['idNovena'];
    //console.log(this.novenaId)
    this.allnovena = this.novenaService.getNovenas();
    this.infoNovena = this.allnovena.find((record) => {
      return record.idNovena == this.novenaId;
    });
    console.log(this.infoNovena);
   }

  ngOnInit() {

  }

  // slides = [
  //   {img: "assets/img/todias.png", routerlink: "/compartir-novena"},
  //   {img: "assets/img/virgen.png", routerlink: "/organiza-novena"},
  //   {img: "assets/img/sjose.png", routerlink: "/eventos"},
  //   {img: "assets/img/gozos.png", routerlink: "https://f63y8.app.goo.gl/claromusica"},
  //   {img: "assets/img/ninojesu.png", routerlink: "/eventos"},
  //   {img: "assets/img/oraciondia.png", routerlink: "/eventos"}
  // ];
  // slideConfig = {"slidesToShow": 5 , "slidesToScroll": 2}

}
