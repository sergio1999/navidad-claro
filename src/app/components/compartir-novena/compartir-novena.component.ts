import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-compartir-novena',
  templateUrl: './compartir-novena.component.html',
  styleUrls: ['./compartir-novena.component.sass']
})
export class CompartirNovenaComponent implements OnInit {
  myformulario: FormGroup;
  submitted  = false;
  infoForm: any;
  resumen = false;
  crear = true;
  lista = [];

  
  date = new Date();
  tt = this.date.getTime();
  dif = this.date.getTimezoneOffset()*60000;
  today =new Date(this.tt - this.dif).toJSON().split('T')[0];
  
  constructor(private formBuilder: FormBuilder, private route: Router) { }
  get form(){ return this.myformulario.controls;}
  ngOnInit() {
    this.myformulario = this.formBuilder.group({
      nombre: ['', Validators.required ],
      fecha: ['', Validators.required ],
      hora: [ '', Validators.required ],
      lugar: ['', Validators.required ],
      llevas: ['', Validators.required]
    });

    this.getNovenas();
  }

  onSubmit(){
    this.submitted  = true;
    if(this.myformulario.invalid){
      return
    }
    else{
      this.getNovenas();
      this.resumen = true;
      this.crear = false;
      this.infoForm = this.myformulario.value;
      this.lista.push(this.infoForm);
      localStorage.setItem('listaNovenas', JSON.stringify(this.lista));
    }
  }

  getNovenas(){
    let temp = JSON.parse(localStorage.getItem("listaNovenas"));
    if (temp != undefined) {
      this.lista = temp;
    }
  }
 
}
