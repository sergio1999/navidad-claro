import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompartirNovenaComponent } from './compartir-novena.component';

describe('CompartirNovenaComponent', () => {
  let component: CompartirNovenaComponent;
  let fixture: ComponentFixture<CompartirNovenaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompartirNovenaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompartirNovenaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
