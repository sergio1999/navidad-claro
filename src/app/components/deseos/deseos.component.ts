import { Component, OnInit } from '@angular/core';
import { Deseos } from 'src/app/interfaces/deseos';
import { DeseosService } from 'src/app/services/deseos.service';

@Component({
  selector: 'app-deseos',
  templateUrl: './deseos.component.html',
  styleUrls: ['./deseos.component.sass']
})
export class DeseosComponent implements OnInit {
  deseos: Deseos[] = [];
  alldeseos: Deseos[] = [];
  todosdeseos: Deseos[] = [];

  cantDeseos = 6;
  comienzoDeseos = 0;
  finDeseos = this.cantDeseos;
  hayMas = true;

  constructor(private deseosService: DeseosService) { 
    this.deseosService.obtenerDeseos(this.cantDeseos).valueChanges().subscribe((data: Deseos[]) =>{
      this.deseos = data;
      console.log(this.deseos);
    }, (error)=>{
      console.log(error);
    });
    this.deseosService.obtenerDeseos(-1).valueChanges().subscribe((data: Deseos[]) =>{
      this.alldeseos = data;
      
    }, (error)=>{
      console.log(error);
    });
    this.deseosService.getdeseos().valueChanges().subscribe((data: Deseos[]) => {
      this.todosdeseos = data;
      console.log(this.todosdeseos);
      }, (error)=>{
      console.log(error)
    });
  }

  verMas(){
    if(this.finDeseos < this.deseos.length){
      this.finDeseos = this.finDeseos + this.cantDeseos;
      if(this.finDeseos > this.deseos.length){
        this.finDeseos = this.deseos.length;
        this.hayMas = false;
      }
    }else{
      this.hayMas = false;
    }
  }

  ngOnInit() {
  }

}
