import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizaNovenaComponent } from './organiza-novena.component';

describe('OrganizaNovenaComponent', () => {
  let component: OrganizaNovenaComponent;
  let fixture: ComponentFixture<OrganizaNovenaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganizaNovenaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizaNovenaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
