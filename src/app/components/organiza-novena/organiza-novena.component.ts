import { Component, OnInit } from '@angular/core';
import { Novenas } from 'src/app/interfaces/novenas';
import { NovenasService } from 'src/app/services/novenas.service';


@Component({
  selector: 'app-organiza-novena',
  templateUrl: './organiza-novena.component.html',
  styleUrls: ['./organiza-novena.component.sass']
})
export class OrganizaNovenaComponent implements OnInit {
  allnovena: Novenas[];
  constructor(private novenaService: NovenasService) { 
    this.allnovena = novenaService.getNovenas();
  
  }

  ngOnInit() {
  }

}
