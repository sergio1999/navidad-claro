import { Injectable } from '@angular/core';
import { Eventos } from '../interfaces/eventos';

@Injectable({
  providedIn: 'root'
})
export class EventosService {
  eventos: Eventos[]
  constructor() { 
    let evento1: Eventos = {
      idevento: 1,
      imgDest: "assets/img/SANTAMARTA-100.jpg",
      lugar: "CC Ocean Mall Santa Marta",
      nombreEvent: "CC Ocean Mall Santa Marta",
      descrip: "¡Tendremos nuestras novenas Claro para llenar nuestra vida de Navidad! Rezaremos la tradicional novena de aguinaldos acompañados de un coro musical; contaremos con artistas y presentadores invitados de alto nivel, sorteos de premios (smartphones y kits de merchandising) y obsequiaremos a los asistentes buñuelos y natilla.",
      puntosEventos: [
        "Pendiente",
      ],
      hora: "6:00 P.M.",
      fecha: "22/12/2018",
      datalat: 11.23194,
      datalng: -74.2019615,
      direccion: "Ferrocaril #15-100, Santa Marta"
    }
    let evento2: Eventos = {
      idevento: 2,
      imgDest: "assets/img/CARTAGENA-100.jpg",
      lugar: "CC Ejecutivos Cartagena",
      nombreEvent: "CC Ejecutivos Cartagena",
      descrip: "¡Tendremos nuestras novenas Claro para llenar nuestra vida de Navidad! Rezaremos la tradicional novena de aguinaldos acompañados de un coro musical; contaremos con artistas y presentadores invitados de alto nivel, sorteos de premios (smartphones y kits de merchandising) y obsequiaremos a los asistentes buñuelos y natilla.",
      puntosEventos: [
        "Pendiente",
      ],
      hora: "6:00 P.M.",
      fecha: "22/12/2018",
      datalat: 10.39909,
      datalng: -75.4942212,
      direccion: "Cl. 31 N° 57 - 106, Cartagena, Bolívar"
    }
    let evento3: Eventos = {
      idevento: 3,
      imgDest: "assets/img/LADORADA-100.jpg",
      lugar: "Polideportivo las Ferias La Dorada",
      nombreEvent: "Polideportivo las Ferias La Dorada",
      descrip: "¡Tendremos nuestras novenas Claro para llenar nuestra vida de Navidad! Rezaremos la tradicional novena de aguinaldos acompañados de un coro musical; contaremos con artistas y presentadores invitados de alto nivel, sorteos de premios (smartphones y kits de merchandising) y obsequiaremos a los asistentes buñuelos y natilla.",
      puntosEventos: [
        "Apoyo Tropicana Estéreo.",
      ],
      hora: "6:00 P.M.",
      fecha: "22/12/2018",
      datalat: 5.4754393,
      datalng: -74.6797764,
      direccion: "La Dorada, Caldas"
    }
    let evento4: Eventos = {
      idevento: 4,
      imgDest: "assets/img/QUIBDO-100.jpg",
      lugar: "Parqueue Manuel Mosquera Quibdó",
      nombreEvent: "Parqueue Manuel Mosquera",
      descrip: "¡Tendremos nuestras novenas Claro para llenar nuestra vida de Navidad! Rezaremos la tradicional novena de aguinaldos acompañados de un coro musical; contaremos con artistas y presentadores invitados de alto nivel, sorteos de premios (smartphones y kits de merchandising) y obsequiaremos a los asistentes buñuelos y natilla.",
      puntosEventos: [
        "Pte confirmar Apoyo de Emisora de la Policía.",
      ],
      hora: "6:00 P.M",
      fecha: "21/12/2018",
      datalat: 5.6945367,
      datalng: -76.6627265,
      direccion: "# a 3-50, Cl. 31 #32"
    }
    let evento5: Eventos = {
      idevento: 5,
      imgDest: "assets/img/PIEDECUESTA-100.jpg",
      lugar: "Parqueue Principal Piedecuesta",
      nombreEvent: "Parqueue Principal Piedecuesta",
      descrip: "¡Tendremos nuestras novenas Claro para llenar nuestra vida de Navidad! Rezaremos la tradicional novena de aguinaldos acompañados de un coro musical; contaremos con artistas y presentadores invitados de alto nivel, sorteos de premios (smartphones y kits de merchandising) y obsequiaremos a los asistentes buñuelos y natilla.",
      puntosEventos: [
        "Apoyo Tropicana Estéreo.",
      ],
      hora: "6:00 P.M.",
      fecha: "22/12/2018",
      datalat: 6.9845212,
      datalng: -73.0553135,
      direccion: "Piedecuesta Santander"
    }
    let evento6: Eventos = {
      idevento: 6,
      imgDest: "assets/img/IBAGUE-100.jpg",
      lugar: "CC Multicentro Ibagué",
      nombreEvent: "CC Multicentro Ibagué",
      descrip: "¡Tendremos nuestras novenas Claro para llenar nuestra vida de Navidad! Rezaremos la tradicional novena de aguinaldos acompañados de un coro musical; contaremos con artistas y presentadores invitados de alto nivel, sorteos de premios (smartphones y kits de merchandising) y obsequiaremos a los asistentes buñuelos y natilla.",
      puntosEventos: [
        "Apoyo Tropicana Estéreo.",
      ],
      hora: "6:00 P.M.",
      fecha: "22/12/2018",
      datalat: 4.4366033,
      datalng: -75.2042717,
      direccion: "Avenida 5 Carrera 6, Ibague"
    }
    let evento7: Eventos = {
      idevento: 9,
      imgDest: "assets/img/PASTO-100.jpg",
      lugar: "CC Polideportivo Pasto",
      nombreEvent: "CC Polideportivo Pasto",
      descrip: "¡Tendremos nuestras novenas Claro para llenar nuestra vida de Navidad! Rezaremos la tradicional novena de aguinaldos acompañados de un coro musical; contaremos con artistas y presentadores invitados de alto nivel, sorteos de premios (smartphones y kits de merchandising) y obsequiaremos a los asistentes buñuelos y natilla.",
      puntosEventos: [
        "Apoyo Tropicana Estéreo.",
      ],
      hora: "6:00 P.M.",
      fecha: "22/12/2018",
      datalat: 1.2163381,
      datalng: -77.2908954,
      direccion: "Calle 11 No. 34 - 78"
    } 
    let evento8: Eventos = {
      idevento: 8,
      imgDest: "assets/img/SOACHA-100.jpg",
      lugar: "CC Ventura Terreros",
      nombreEvent: "CC Ventura Terreros",
      descrip: "¡Tendremos nuestras novenas Claro para llenar nuestra vida de Navidad! Rezaremos la tradicional novena de aguinaldos acompañados de un coro musical; contaremos con artistas y presentadores invitados de alto nivel, sorteos de premios (smartphones y kits de merchandising) y obsequiaremos a los asistentes buñuelos y natilla.",
      puntosEventos: [
        "Apoyo Tropicana Estéreo.",
      ],
      hora: "4:00 P.M. / 8:00 P.M.",
      fecha: "22/12/2018",
      datalat: 4.5858368,
      datalng: -74.194364,
      direccion: "Soacha Cundinamarca"
    }
    let evento10: Eventos = {
      idevento: 10,
      imgDest: "assets/img/CUCUTA-100.jpg",
      lugar: "Parque Principal Barrio Atalaya",
      nombreEvent: "Parque Principal Barrio Atalaya Cucuta",
      descrip: "¡Tendremos nuestras novenas Claro para llenar nuestra vida de Navidad! Rezaremos la tradicional novena de aguinaldos acompañados de un coro musical; contaremos con artistas y presentadores invitados de alto nivel, sorteos de premios (smartphones y kits de merchandising) y obsequiaremos a los asistentes buñuelos y natilla.",
      hora: "6:00 P.M.",
      fecha: "16/12/2018",
      datalat: 7.9127939,
      datalng: -72.5280583,
      direccion: "Barrio Atalaya Cucuta"
    }
    let evento11: Eventos = {
      idevento: 11,
      imgDest: "assets/img/RICAURTE-100.jpg",
      lugar: "Parque de la Igelsia Antigua Ricaurte",
      nombreEvent: "Parque de la Igelsia Antigua Cundinamarca",
      descrip: "¡Tendremos nuestras novenas Claro para llenar nuestra vida de Navidad! Rezaremos la tradicional novena de aguinaldos acompañados de un coro musical; contaremos con artistas y presentadores invitados de alto nivel, sorteos de premios (smartphones y kits de merchandising) y obsequiaremos a los asistentes buñuelos y natilla.",
      hora: "6:00 P.M.",
      fecha: "21/12/2018",
      datalat: 4.2801102,
      datalng: -74.8092741,
      direccion: "Ricaurte Cundinamarca"
    }
    let evento12: Eventos = {
      idevento: 12,
      imgDest: "assets/img/TUNJA-100.jpg",
      lugar: "CC Unicentro Tunja",
      nombreEvent: "CC Unicentro Tunja",
      descrip: "¡Tendremos nuestras novenas Claro para llenar nuestra vida de Navidad! Rezaremos la tradicional novena de aguinaldos acompañados de un coro musical; contaremos con artistas y presentadores invitados de alto nivel, sorteos de premios (smartphones y kits de merchandising) y obsequiaremos a los asistentes buñuelos y natilla.",
      hora: "5:00 P.M.",
      fecha: "21/12/2018",
      datalat: 5.5462969,
      datalng: -73.3514267,
      direccion: "Avenida Universitaria 39 #77, Tunja, Boyacá"
    }
    let evento13: Eventos = {
      idevento: 13,
      imgDest: "assets/img/ZIPAQUIRA-100.jpg",
      lugar: "Parque La Esperanza Zipaquirá",
      nombreEvent: "Parque La Esperanza Zipaquirá",
      descrip: "¡Tendremos nuestras novenas Claro para llenar nuestra vida de Navidad! Rezaremos la tradicional novena de aguinaldos acompañados de un coro musical; contaremos con artistas y presentadores invitados de alto nivel, sorteos de premios (smartphones y kits de merchandising) y obsequiaremos a los asistentes buñuelos y natilla.",
      hora: "6:00 P.M.",
      fecha: "18/12/2018",
      datalat: 5.0206618,
      datalng: -74.0024141,
      direccion: "Zipaquirá, Cundinamarca"
    }
     
    this.eventos = [evento1, evento2, evento3, evento4, evento5, evento6, evento7, evento8, evento10, evento11, evento12, evento13]; 
  }
  getEventos(){
    return this.eventos;
  }
}
