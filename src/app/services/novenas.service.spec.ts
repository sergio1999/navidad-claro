import { TestBed } from '@angular/core/testing';

import { NovenasService } from './novenas.service';

describe('NovenasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NovenasService = TestBed.get(NovenasService);
    expect(service).toBeTruthy();
  });
});
