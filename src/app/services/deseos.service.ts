import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Deseos } from '../interfaces/deseos';

@Injectable({
  providedIn: 'root'
})
export class DeseosService {
  API_ENDPOINT: string ='https://miclaroapp.com.co/api/index.php/v1/core/web/Sesion.json';
  //tokenUser: string;
  constructor(private angularFireDatabase: AngularFireDatabase, private httpClient: HttpClient) { }
  addDeseo(deseos){
    let id = new Date().getTime();
    return this.angularFireDatabase.object('/Deseos/' + id ).set(deseos);
  }
  obtenerDeseos(cant){
    cant = cant?cant:18;
    if(cant == -1){
      return this.angularFireDatabase.list('/Deseos',ref => ref.orderByChild('activo').equalTo('1'));
    }else{
      //return this.angularFireDatabase.list('/Deseos',ref => ref.limitToFirst(cant).orderByChild('activo').equalTo('1'));
      return this.angularFireDatabase.list('/Deseos',ref => ref.orderByChild('activo').equalTo('1'));
    }
    
  }
  getdeseos(){
    return this.angularFireDatabase.list('/Deseos');
  }
  /*save(deseos: Deseos){
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.httpClient.post(this.API_ENDPOINT + '/deseos', deseos, {headers: headers});
  }*/
  post(tokenUser:string){
    var data = {
      "data":{
        "accion":"dec",
        "token": tokenUser
      }
    };
    return this.httpClient.post(this.API_ENDPOINT,data,{});
  }
}
