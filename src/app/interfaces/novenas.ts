export interface Novenas {
    idNovena: string;
    fecha: string;
    oracion: string;
    img: string;
    titulo?: string
}

