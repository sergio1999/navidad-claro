export interface Eventos {
    idevento: number;
    imgDest: string;
    lugar: string;
    nombreEvent: string;
    descrip: string;
    puntosEventos?: string [];
    hora: string;
    fecha: string;
    datalat: number;
    datalng: number;
    direccion: string;
}
