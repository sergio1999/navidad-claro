import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { InicioComponent } from './components/inicio/inicio.component';
import { CompartirNovenaComponent } from './components/compartir-novena/compartir-novena.component';
import { DeseosComponent } from './components/deseos/deseos.component';
import { EventosComponent } from './components/eventos/eventos.component';
import { OrganizaNovenaComponent } from './components/organiza-novena/organiza-novena.component';
import { ResumenNovenaComponent } from './components/resumen-novena/resumen-novena.component';
import { NovenasComponent } from './components/novenas/novenas.component';
import { EventoComponent } from './components/evento/evento.component';

const routes: Routes = [
  { path: '', component: InicioComponent},
  { path: 'inicio', component: InicioComponent},
  { path: 'inicio/:token', component: InicioComponent},
  { path: 'compartir-novena', component: CompartirNovenaComponent }, 
  { path: 'deseos', component: DeseosComponent },
  { path: 'eventos', component: EventosComponent },
  { path: 'evento/:idevento', component: EventoComponent},
  { path: 'organiza-novena', component: OrganizaNovenaComponent},
  { path: 'resumen', component: ResumenNovenaComponent},
  { path: 'novenas/:idNovena', component: NovenasComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routeComponents = [
  InicioComponent,
  CompartirNovenaComponent,
  DeseosComponent,
  EventosComponent,
  OrganizaNovenaComponent,
  NovenasComponent,
  ResumenNovenaComponent,
  EventoComponent
]
